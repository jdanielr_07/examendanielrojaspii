/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examendanielrojaspii.DAO;

import examendanielrojaspii.entities.Receta;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import javax.swing.JOptionPane;


/**
 *
 * @author Allan Murillo
 */
public class RecetaDAO {

    public LinkedList<Receta> cargarListaRecetas(String filtro, char estado) {
        LinkedList<Receta> recetas = new LinkedList<>();
        filtro = generarFiltro(filtro, estado);
        try ( Connection con = Conexion.getConexion()) {
            String sql = "SELECT id, nombre, ingredientes, indicaciones, activo, id_categoria FROM recetas ";
            sql += filtro;
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                recetas.add(cargarReceta(rs));
            }
            stmt.clearParameters();
            return recetas;
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
    }



    private Receta cargarReceta(ResultSet rs) throws SQLException {
        Receta r = new Receta();
        r.setId(rs.getInt("id"));
        r.setNombre(rs.getString("nombre"));
        r.setIndicaciones(rs.getString("indicaciones"));
        r.setIngredientes(rs.getString("ingredientes"));
        r.setActivo(rs.getBoolean("activo"));          
        cargar(r, rs);
        return r;
    }

    private void cargar(Receta r, ResultSet rs) throws SQLException {
        r.setId(rs.getInt("id"));
        r.setNombre(rs.getString("nombre"));
        r.setIndicaciones(rs.getString("indicaciones"));
        r.setIngredientes(rs.getString("ingredientes"));
        r.setActivo(rs.getBoolean("activo"));     
    }

    public void activar(int id, boolean activo) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "update receta set activo = ? where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setBoolean(1, activo);
            stmt.setInt(2, id);
            stmt.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
    }

    public void actualizar(Receta r) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "update recetas set codigo=?, nombre=?, descripcion=?, precio=?, estado=?, %s where id = ?";
            PreparedStatement stmt = null;
            stmt.setString(1, r.getNombre());
            stmt.setString(2, r.getIngredientes());
            stmt.setString(3, r.getIndicaciones());
            stmt.setBoolean(4, r.isActivo());
            stmt.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
    }

    public void insertar(Receta r) {
        try ( Connection con = Conexion.getConexion()) {
            String sql = "INSERT INTO recetas (nombre, ingredientes, indicaciones, activo, id_categoria) "
                    + "VALUES (?, ?, ?, ?, ?)";
            PreparedStatement stmt = con.prepareStatement(sql); 
            stmt.setString(1, r.getNombre());
            stmt.setString(2, r.getIngredientes());
            stmt.setString(3, r.getIndicaciones());
            stmt.setBoolean(4, r.isActivo());
            stmt.setInt(5, r.getId_categoria());
            System.out.println(stmt);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null,"Receta registadra con éxito");
        } catch (Exception e) {
            throw new RuntimeException("Problemas de conexión con el servidor");
        }
    }

    private String generarFiltro(String filtro, char estado) {
        String txt = "";
        if (filtro.isBlank()) {
            if (estado != 'T') {
                txt = String.format(" where estado = %s", (estado == 'A' ? "true" : "false"));
            }
        } else if (!filtro.isBlank()) {
            txt = " where (lower(codigo) like lower('%s') or  lower(nombre) like lower('%s') or  lower(descripcion) like lower('%s'))";
            String dato = "%" + filtro.trim() + "%";
            txt = String.format(txt, dato, dato, dato);
            if (estado != 'T') {
                txt += String.format(" and (estado = %s)", (estado == 'A' ? "true" : "false"));
            }
        }
        return txt;
    }
}
