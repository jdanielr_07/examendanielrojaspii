/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examendanielrojaspii.entities;

import java.util.LinkedList;

/**
 *
 * @author Daniel
 */
public class Receta extends Categoria{
    private int id;
    private String nombre;
    private String ingredientes;
    private String indicaciones;
    private boolean activo;
    private int id_categoria;
    protected LinkedList<Receta> recetas;
    
    public Receta() {
        nombre = "";
        ingredientes = "";
        indicaciones = "";
        recetas = new LinkedList<>();      
    }

    public Receta(int id, String nombre, String ingredientes, String indicaciones, boolean activo, int id_categoria, LinkedList<Receta> recetas) {
        this.id = id;
        this.nombre = nombre;
        this.ingredientes = ingredientes;
        this.indicaciones = indicaciones;
        this.activo = activo;
        this.id_categoria = id_categoria;
        this.recetas = recetas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(String ingredientes) {
        this.ingredientes = ingredientes;
    }

    public String getIndicaciones() {
        return indicaciones;
    }

    public void setIndicaciones(String indicaciones) {
        this.indicaciones = indicaciones;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public int getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(int id_categoria) {
        this.id_categoria = id_categoria;
    }

    public LinkedList<Receta> getRecetas() {
        return recetas;
    }

    public void setRecetas(LinkedList<Receta> recetas) {
        this.recetas = recetas;
    }

    @Override
    public String toString() {
        return "Receta{" + "id=" + id + ", nombre=" + nombre + ", ingredientes=" + ingredientes + ", indicaciones=" + indicaciones + ", activo=" + activo + ", id_categoria=" + id_categoria + ", recetas=" + recetas + '}';
    }
}
