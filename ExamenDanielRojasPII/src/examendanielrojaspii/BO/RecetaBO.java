/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examendanielrojaspii.BO;

import examendanielrojaspii.DAO.RecetaDAO;
import examendanielrojaspii.entities.Receta;
import java.util.LinkedList;


/**
 *
 * @author Allan Murillo
 */
public class RecetaBO {

    public LinkedList<Receta> cargarArticulos(String filtro, char estado) {
        return new RecetaDAO().cargarListaRecetas(filtro.trim(), estado);
    }

    public void eliminar(int id) {
        if (id > 0) {
            new RecetaDAO().activar(id, false);
        } else {
            throw new RuntimeException("Favor seleccione un artículo 1");
        }
    }
    
    public void activar(int id) {
        if (id > 0) {
            new RecetaDAO().activar(id, true);
        } else {
            throw new RuntimeException("Favor seleccione un artículo 2");
        }
    }

    public void guardar(Receta art) {
        // validaciones respectepivas del articulo
         if (art == null) {
            throw new RuntimeException("Datos inválidos!!");
        }

        if (art.getNombre().isBlank()) {
            throw new RuntimeException("Se requiere un nombre");
        }

        if (art.getIndicaciones().isBlank()) {
            throw new RuntimeException("Ingrese las indicaciones");
        }

        if (art.getIngredientes().isEmpty()) {
            throw new RuntimeException("Ingrese los ingredientes");
        }

        if (art.getId_categoria()==0) {
            throw new RuntimeException("Elija una categoria de comida");
        }
        if(art.getId()> 0){
            new RecetaDAO().actualizar(art);
        } else {
            new RecetaDAO().insertar(art);
        }
    }

}
